package main

import (
	"log"

	"github.com/gin-contrib/cors"
	"github.com/gin-gonic/gin"
	"gitlab.com/delivery-go-react/back/gateway/config"
	"gitlab.com/delivery-go-react/back/gateway/pkg/restaurant"
	"gitlab.com/delivery-go-react/back/gateway/pkg/role"
	"gitlab.com/delivery-go-react/back/gateway/pkg/user"
)

func main() {
	c, err := config.LoadConfig()

	if err != nil {
		log.Fatalln("Failed at config", err)
	}

	r := gin.Default()

	config := cors.DefaultConfig()
	config.AllowOrigins = []string{"http://localhost:3000"} // Allow your Next.js app's domain
	config.AllowCredentials = true
	config.AllowMethods = []string{"GET", "POST", "PUT", "DELETE"}
	config.AllowHeaders = []string{"Origin", "Content-Type", "Accept", "Authorization"}

	r.Use(cors.New(config))

	userSvc := *user.RegisterRoutes(r, &c)
	roleSvc := role.RegisterRoutes(r, &c, &userSvc)
	restaurant.RegisterRoutes(r, &c, &userSvc, roleSvc)

	r.Run(c.Port)
}
