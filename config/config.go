package config

import "github.com/spf13/viper"

type Config struct {
	Port             string `mapstructure:"PORT"`
	UserSvcUrl       string `mapstructure:"USER_SVC_URL"`
	RoleSvcUrl       string `mapstructure:"ROLE_SVC_URL"`
	RestaurantSvcUrl string `mapstructure:"RESTAURANT_SVC_URL"`
	AllowUrl         string `mapstructure:"ALLOW_URL"`
}

func LoadConfig() (c Config, err error) {
	viper.AddConfigPath("./config/envs")
	viper.SetConfigName("dev")
	viper.SetConfigType("env")

	viper.AutomaticEnv()

	err = viper.ReadInConfig()

	if err != nil {
		return
	}

	err = viper.Unmarshal(&c)

	return
}
