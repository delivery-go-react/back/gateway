proto:
	protoc ./pkg/user/pb/*.proto --go-grpc_out=require_unimplemented_servers=false:./ --go_out=./
	protoc ./pkg/role/pb/*.proto --go-grpc_out=require_unimplemented_servers=false:./ --go_out=./
	protoc ./pkg/restaurant/pb/*.proto --go-grpc_out=require_unimplemented_servers=false:./ --go_out=./

server:
	go run cmd/main.go