package routes

import (
	"context"
	"net/http"

	"github.com/gin-gonic/gin"
	"gitlab.com/delivery-go-react/back/gateway/pkg/restaurant/pb"
)

type UpdateRestaurantRequestBody struct {
	Id          int64
	Name        string
	Description string
	Address     string
	Phone       string
	Email       string
}

func UpdateRestaurant(ctx *gin.Context, c pb.RestaurantServiceClient) {
	body := UpdateRestaurantRequestBody{}

	if err := ctx.BindJSON(&body); err != nil {
		ctx.AbortWithError(http.StatusBadRequest, err)
		return
	}

	res, err := c.UpdateRestaurant(context.Background(), &pb.UpdateRestaurantRequest{
		Id:          body.Id,
		Name:        body.Name,
		Description: body.Description,
		Address:     body.Address,
		Phone:       body.Phone,
		Email:       body.Email,
	})

	if err != nil {
		ctx.AbortWithError(http.StatusBadGateway, err)
		return
	}

	ctx.JSON(http.StatusCreated, &res)
}
