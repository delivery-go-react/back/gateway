package routes

import (
	"context"
	"net/http"

	"github.com/gin-gonic/gin"
	"gitlab.com/delivery-go-react/back/gateway/pkg/restaurant/pb"
)

func GetAllRestaurants(ctx *gin.Context, c pb.RestaurantServiceClient) {
	res, err := c.GetRestaurants(context.Background(), &pb.GetRestaurantsRequest{})

	if err != nil {
		ctx.AbortWithError(http.StatusBadGateway, err)
		return
	}

	ctx.JSON(int(res.Status), &res)
}
