package routes

import (
	"context"
	"net/http"

	"github.com/gin-gonic/gin"
	"gitlab.com/delivery-go-react/back/gateway/pkg/restaurant/pb"
)

type DeletePositionRequestBody struct {
	Id int64
}

func DeletePosition(ctx *gin.Context, c pb.RestaurantServiceClient) {
	body := DeletePositionRequestBody{}

	if err := ctx.BindJSON(&body); err != nil {
		ctx.AbortWithError(http.StatusBadRequest, err)
		return
	}

	res, err := c.DeletePosition(context.Background(), &pb.DeletePositionRequest{
		Id: body.Id,
	})

	if err != nil {
		ctx.AbortWithError(http.StatusBadGateway, err)
		return
	}

	ctx.JSON(http.StatusCreated, &res)
}
