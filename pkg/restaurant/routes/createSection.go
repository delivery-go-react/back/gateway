package routes

import (
	"context"
	"net/http"

	"github.com/gin-gonic/gin"
	"gitlab.com/delivery-go-react/back/gateway/pkg/restaurant/pb"
)

type CreateSectionRequestBody struct {
	MenuId int64
	Name   string
}

func CreateSection(ctx *gin.Context, c pb.RestaurantServiceClient) {
	body := CreateSectionRequestBody{}

	if err := ctx.BindJSON(&body); err != nil {
		ctx.AbortWithError(http.StatusBadRequest, err)
		return
	}

	res, err := c.CreateSection(context.Background(), &pb.CreateSectionRequest{
		MenuId: body.MenuId,
		Name:   body.Name,
	})

	if err != nil {
		ctx.AbortWithError(http.StatusBadGateway, err)
		return
	}

	ctx.JSON(http.StatusCreated, &res)
}
