package routes

import (
	"context"
	"net/http"

	"github.com/gin-gonic/gin"
	"gitlab.com/delivery-go-react/back/gateway/pkg/restaurant/pb"
)

type CreateMenuRequestBody struct {
	RestaurantId int64
	Name         string
	Description  string
}

func CreateMenu(ctx *gin.Context, c pb.RestaurantServiceClient) {
	body := CreateMenuRequestBody{}

	if err := ctx.BindJSON(&body); err != nil {
		ctx.AbortWithError(http.StatusBadRequest, err)
		return
	}

	res, err := c.CreateMenu(context.Background(), &pb.CreateMenuRequest{
		RestaurantId: body.RestaurantId,
		Name:         body.Name,
		Description:  body.Description,
	})

	if err != nil {
		ctx.AbortWithError(http.StatusBadGateway, err)
		return
	}

	ctx.JSON(http.StatusCreated, &res)
}
