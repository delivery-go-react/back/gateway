package routes

import (
	"context"
	"net/http"

	"github.com/gin-gonic/gin"
	"gitlab.com/delivery-go-react/back/gateway/pkg/restaurant/pb"
)

type CreatePositionRequestBody struct {
	SectionId int64
	Name      string
	Price     float32
	Image     string
}

func CreatePosition(ctx *gin.Context, c pb.RestaurantServiceClient) {
	body := CreatePositionRequestBody{}

	if err := ctx.BindJSON(&body); err != nil {
		ctx.AbortWithError(http.StatusBadRequest, err)
		return
	}

	res, err := c.CreatePosition(context.Background(), &pb.CreatePositionRequest{
		SectionId: body.SectionId,
		Name:      body.Name,
		Price:     body.Price,
		Image:     body.Image,
	})

	if err != nil {
		ctx.AbortWithError(http.StatusBadGateway, err)
		return
	}

	ctx.JSON(http.StatusCreated, &res)
}
