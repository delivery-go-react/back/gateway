package routes

import (
	"context"
	"net/http"

	"github.com/gin-gonic/gin"
	"gitlab.com/delivery-go-react/back/gateway/pkg/restaurant/pb"
)

type UpdateSectionRequestBody struct {
	Id   int64
	Name string
}

func UpdateSection(ctx *gin.Context, c pb.RestaurantServiceClient) {
	body := UpdateSectionRequestBody{}

	if err := ctx.BindJSON(&body); err != nil {
		ctx.AbortWithError(http.StatusBadRequest, err)
		return
	}

	res, err := c.UpdateSection(context.Background(), &pb.UpdateSectionRequest{
		Id:   body.Id,
		Name: body.Name,
	})

	if err != nil {
		ctx.AbortWithError(http.StatusBadGateway, err)
		return
	}

	ctx.JSON(http.StatusCreated, &res)
}
