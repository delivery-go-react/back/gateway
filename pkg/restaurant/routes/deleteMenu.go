package routes

import (
	"context"
	"net/http"

	"github.com/gin-gonic/gin"
	"gitlab.com/delivery-go-react/back/gateway/pkg/restaurant/pb"
)

type DeleteMenuRequestBody struct {
	Id int64
}

func DeleteMenu(ctx *gin.Context, c pb.RestaurantServiceClient) {
	body := UpdateMenuRequestBody{}

	if err := ctx.BindJSON(&body); err != nil {
		ctx.AbortWithError(http.StatusBadRequest, err)
		return
	}

	res, err := c.DeleteMenu(context.Background(), &pb.DeleteMenuRequest{
		Id: body.Id,
	})

	if err != nil {
		ctx.AbortWithError(http.StatusBadGateway, err)
		return
	}

	ctx.JSON(http.StatusCreated, &res)
}
