package routes

import (
	"context"
	"net/http"

	"github.com/gin-gonic/gin"
	"gitlab.com/delivery-go-react/back/gateway/pkg/restaurant/pb"
)

type UpdatePositionRequestBody struct {
	Id    int64
	Name  string
	Price float32
	Image string
}

func UpdatePosition(ctx *gin.Context, c pb.RestaurantServiceClient) {
	body := UpdatePositionRequestBody{}

	if err := ctx.BindJSON(&body); err != nil {
		ctx.AbortWithError(http.StatusBadRequest, err)
		return
	}

	res, err := c.UpdatePosition(context.Background(), &pb.UpdatePositionRequest{
		Id:    body.Id,
		Name:  body.Name,
		Price: body.Price,
	})

	if err != nil {
		ctx.AbortWithError(http.StatusBadGateway, err)
		return
	}

	ctx.JSON(http.StatusCreated, &res)
}
