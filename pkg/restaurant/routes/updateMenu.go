package routes

import (
	"context"
	"net/http"

	"github.com/gin-gonic/gin"
	"gitlab.com/delivery-go-react/back/gateway/pkg/restaurant/pb"
)

type UpdateMenuRequestBody struct {
	Id          int64
	Name        string
	Description string
}

func UpdateMenu(ctx *gin.Context, c pb.RestaurantServiceClient) {
	body := UpdateMenuRequestBody{}

	if err := ctx.BindJSON(&body); err != nil {
		ctx.AbortWithError(http.StatusBadRequest, err)
		return
	}

	res, err := c.UpdateMenu(context.Background(), &pb.UpdateMenuRequest{
		Id:          body.Id,
		Name:        body.Name,
		Description: body.Description,
	})

	if err != nil {
		ctx.AbortWithError(http.StatusBadGateway, err)
		return
	}

	ctx.JSON(http.StatusCreated, &res)
}
