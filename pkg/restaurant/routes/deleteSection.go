package routes

import (
	"context"
	"net/http"

	"github.com/gin-gonic/gin"
	"gitlab.com/delivery-go-react/back/gateway/pkg/restaurant/pb"
)

type DeleteSectionRequestBody struct {
	Id int64
}

func DeleteSection(ctx *gin.Context, c pb.RestaurantServiceClient) {
	body := DeleteSectionRequestBody{}

	if err := ctx.BindJSON(&body); err != nil {
		ctx.AbortWithError(http.StatusBadRequest, err)
		return
	}

	res, err := c.DeleteSection(context.Background(), &pb.DeleteSectionRequest{
		Id: body.Id,
	})

	if err != nil {
		ctx.AbortWithError(http.StatusBadGateway, err)
		return
	}

	ctx.JSON(http.StatusCreated, &res)
}
