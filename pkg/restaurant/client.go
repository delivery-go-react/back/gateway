package restaurant

import (
	"fmt"

	"gitlab.com/delivery-go-react/back/gateway/config"
	"gitlab.com/delivery-go-react/back/gateway/pkg/restaurant/pb"
	"google.golang.org/grpc"
)

type ServiceClient struct {
	Client pb.RestaurantServiceClient
}

func InitServiceClient(c *config.Config) pb.RestaurantServiceClient {
	cc, err := grpc.Dial(c.RestaurantSvcUrl, grpc.WithInsecure())

	if err != nil {
		fmt.Println("Could not connect:", err)
	}

	return pb.NewRestaurantServiceClient(cc)
}
