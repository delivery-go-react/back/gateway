package restaurant

import (
	"github.com/gin-gonic/gin"
	"gitlab.com/delivery-go-react/back/gateway/config"
	"gitlab.com/delivery-go-react/back/gateway/pkg/restaurant/routes"
	"gitlab.com/delivery-go-react/back/gateway/pkg/role"
	"gitlab.com/delivery-go-react/back/gateway/pkg/user"
)

func RegisterRoutes(r *gin.Engine, c *config.Config, userSvc *user.ServiceClient, roleSvc *role.ServiceClient) *ServiceClient {
	svc := &ServiceClient{
		Client: InitServiceClient(c),
	}

	a := user.InitAuthMiddleware(userSvc)
	p := role.InitPermissionMiddleware(roleSvc)

	routes := r.Group("/")
	routes.Use(a.AuthRequired)

	// restaurant
	routes.GET("/restaurants", svc.GetAllRestaurants)
	routes.GET("/restaurant/:id", svc.GetRestaurant)

	protectedRoutes := r.Group("/")
	protectedRoutes.Use(a.AuthRequired)
	protectedRoutes.Use(p.PermissionRequired)

	// restaurant
	protectedRoutes.POST("/restaurant", svc.CreateRestaurant)
	protectedRoutes.PUT("/restaurant", svc.UpdateRestaurant)

	// menu
	protectedRoutes.GET("/menu/:id", svc.GetMenu)
	protectedRoutes.POST("/menu", svc.CreateMenu)
	protectedRoutes.PUT("/menu", svc.UpdateMenu)
	protectedRoutes.DELETE("/menu", svc.DeleteMenu)

	// section
	protectedRoutes.GET("/section/:id", svc.GetSection)
	protectedRoutes.POST("/section", svc.CreateSection)
	protectedRoutes.PUT("/section", svc.UpdateSection)
	protectedRoutes.DELETE("/section", svc.DeleteSection)

	// position
	protectedRoutes.GET("/position/:id", svc.GetPosition)
	protectedRoutes.POST("/position", svc.CreatePosition)
	protectedRoutes.PUT("/position", svc.UpdatePosition)
	protectedRoutes.DELETE("/position", svc.UpdatePosition)

	return svc
}

// Restaurant

func (svc *ServiceClient) GetAllRestaurants(ctx *gin.Context) {
	routes.GetAllRestaurants(ctx, svc.Client)
}

func (svc *ServiceClient) GetRestaurant(ctx *gin.Context) {
	routes.GetRestaurant(ctx, svc.Client)
}

func (svc *ServiceClient) CreateRestaurant(ctx *gin.Context) {
	routes.CreateRestaurant(ctx, svc.Client)
}

func (svc *ServiceClient) UpdateRestaurant(ctx *gin.Context) {
	routes.UpdateRestaurant(ctx, svc.Client)
}

// menu

func (svc *ServiceClient) CreateMenu(ctx *gin.Context) {
	routes.CreateMenu(ctx, svc.Client)
}

func (svc *ServiceClient) UpdateMenu(ctx *gin.Context) {
	routes.UpdateMenu(ctx, svc.Client)
}

func (svc *ServiceClient) DeleteMenu(ctx *gin.Context) {
	routes.DeleteMenu(ctx, svc.Client)
}

func (svc *ServiceClient) GetMenu(ctx *gin.Context) {
	routes.GetMenu(ctx, svc.Client)
}

// section

func (svc *ServiceClient) CreateSection(ctx *gin.Context) {
	routes.CreateSection(ctx, svc.Client)
}

func (svc *ServiceClient) UpdateSection(ctx *gin.Context) {
	routes.UpdateSection(ctx, svc.Client)
}

func (svc *ServiceClient) DeleteSection(ctx *gin.Context) {
	routes.DeleteSection(ctx, svc.Client)
}

func (svc *ServiceClient) GetSection(ctx *gin.Context) {
	routes.GetSection(ctx, svc.Client)
}

// position

func (svc *ServiceClient) CreatePosition(ctx *gin.Context) {
	routes.CreatePosition(ctx, svc.Client)
}

func (svc *ServiceClient) UpdatePosition(ctx *gin.Context) {
	routes.UpdatePosition(ctx, svc.Client)
}

func (svc *ServiceClient) DeletePosition(ctx *gin.Context) {
	routes.DeletePosition(ctx, svc.Client)
}

func (svc *ServiceClient) GetPosition(ctx *gin.Context) {
	routes.GetPosition(ctx, svc.Client)
}
