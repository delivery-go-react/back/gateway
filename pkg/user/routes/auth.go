package routes

import (
	"context"
	"fmt"
	"net/http"

	"github.com/gin-gonic/gin"
	"gitlab.com/delivery-go-react/back/gateway/pkg/user/pb"
)

func Auth(ctx *gin.Context, c pb.UserServiceClient) {
	token, err := ctx.Cookie("token")
	if err != nil {
		ctx.AbortWithStatus(http.StatusUnauthorized)
		return
	}

	res, err := c.Validate(context.Background(), &pb.ValidateRequest{
		Token: token,
	})

	fmt.Println(res)

	if err != nil || res.Status != http.StatusOK {
		ctx.AbortWithStatus(http.StatusUnauthorized)
		return
	}

	ctx.JSON(http.StatusAccepted, &res)
}
