package routes

import (
	"context"
	"fmt"
	"net/http"

	"github.com/gin-gonic/gin"
	"gitlab.com/delivery-go-react/back/gateway/pkg/user/pb"
)

type LoginRequestBody struct {
	Email    string
	Password string
}

func Login(ctx *gin.Context, c pb.UserServiceClient) {
	b := LoginRequestBody{}

	if err := ctx.BindJSON(&b); err != nil {
		ctx.AbortWithError(http.StatusBadRequest, err)
		return
	}

	res, err := c.Login(context.Background(), &pb.LoginRequest{
		Email:    b.Email,
		Password: b.Password,
	})

	if err != nil {
		ctx.AbortWithError(http.StatusBadGateway, err)
		return
	}

	fmt.Println("Login")
	fmt.Println(res.Token)

	ctx.SetCookie("token", res.Token, 60*60*24, "/", "", false, true)

	ctx.JSON(http.StatusAccepted, &res)
}
