package routes

import (
	"context"
	"net/http"

	"github.com/gin-gonic/gin"
	"gitlab.com/delivery-go-react/back/gateway/pkg/user/pb"
)

type RegisterRequestBody struct {
	Email    string
	Password string
	RoleId   int64
}

func Register(ctx *gin.Context, c pb.UserServiceClient) {
	body := RegisterRequestBody{}

	if err := ctx.BindJSON(&body); err != nil {
		ctx.AbortWithError(http.StatusBadRequest, err)
		return
	}

	res, err := c.Register(context.Background(), &pb.RegisterRequest{
		Email:    body.Email,
		Password: body.Password,
		RoleId:   body.RoleId,
	})

	if err != nil {
		ctx.AbortWithError(http.StatusBadGateway, err)
		return
	}

	ctx.JSON(int(res.Status), &res)
}
