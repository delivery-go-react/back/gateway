package user

import (
	"context"
	"fmt"
	"net/http"

	"github.com/gin-gonic/gin"
	"gitlab.com/delivery-go-react/back/gateway/pkg/user/pb"
)

type AuthMiddlewareConfig struct {
	svc *ServiceClient
}

func InitAuthMiddleware(svc *ServiceClient) AuthMiddlewareConfig {
	return AuthMiddlewareConfig{svc}
}

func (c *AuthMiddlewareConfig) AuthRequired(ctx *gin.Context) {
	token, err := ctx.Cookie("token")
	fmt.Println("AuthMiddleware")
	fmt.Println(token)
	if err != nil {
		ctx.AbortWithStatus(http.StatusUnauthorized)
		return
	}

	fmt.Println("AuthMiddleware")
	fmt.Println(token)

	res, err := c.svc.Client.Validate(context.Background(), &pb.ValidateRequest{
		Token: token,
	})

	if err != nil || res.Status != http.StatusOK {
		ctx.AbortWithStatus(http.StatusUnauthorized)
		return
	}

	fmt.Println("userId", res.UserId)
	fmt.Println("roleId", res.RoleId)

	ctx.Set("userId", res.UserId)
	ctx.Set("roleId", res.RoleId)

	ctx.Next()
}
