package user

import (
	"github.com/gin-gonic/gin"
	"gitlab.com/delivery-go-react/back/gateway/config"
	"gitlab.com/delivery-go-react/back/gateway/pkg/user/routes"
)

func RegisterRoutes(r *gin.Engine, c *config.Config) *ServiceClient {
	svc := &ServiceClient{
		Client: InitServiceClient(c),
	}

	routes := r.Group("/user")
	routes.POST("/register", svc.Register)
	routes.POST("/login", svc.Login)

	routes.GET("/auth", svc.Auth)

	protectedRoutes := r.Group("/user/protected")
	a := InitAuthMiddleware(svc)
	protectedRoutes.Use(a.AuthRequired)
	protectedRoutes.POST("/change-password", svc.ChangePassword)

	return svc
}

func (svc *ServiceClient) Register(ctx *gin.Context) {
	routes.Register(ctx, svc.Client)
}

func (svc *ServiceClient) Login(ctx *gin.Context) {
	routes.Login(ctx, svc.Client)
}

func (svc *ServiceClient) Auth(ctx *gin.Context) {
	routes.Auth(ctx, svc.Client)
}

func (svc *ServiceClient) ChangePassword(ctx *gin.Context) {
	routes.ChangePassword(ctx, svc.Client)
}
