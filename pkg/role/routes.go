package role

import (
	"github.com/gin-gonic/gin"
	"gitlab.com/delivery-go-react/back/gateway/config"
	"gitlab.com/delivery-go-react/back/gateway/pkg/role/routes"
	"gitlab.com/delivery-go-react/back/gateway/pkg/user"
)

func RegisterRoutes(r *gin.Engine, c *config.Config, userSvc *user.ServiceClient) *ServiceClient {
	svc := &ServiceClient{
		Client: InitServiceClient(c),
	}

	routes := r.Group("/role")
	routes.GET("/get-roles", svc.GetAllRoles)

	protectedRoutes := r.Group("/role")
	a := user.InitAuthMiddleware(userSvc)
	p := InitPermissionMiddleware(svc)
	protectedRoutes.Use(a.AuthRequired)
	protectedRoutes.Use(p.PermissionRequired)
	protectedRoutes.GET("/get-permissions", svc.GetAllPermissions)

	return svc
}

func (svc *ServiceClient) GetAllRoles(ctx *gin.Context) {
	routes.GetAllRoles(ctx, svc.Client)
}

func (svc *ServiceClient) GetAllPermissions(ctx *gin.Context) {
	routes.GetAllPermissions(ctx, svc.Client)
}
