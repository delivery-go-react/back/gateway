package role

import (
	"context"
	"fmt"
	"net/http"

	"github.com/gin-gonic/gin"
	"gitlab.com/delivery-go-react/back/gateway/pkg/role/pb"
)

type PermissionMiddlewareConfig struct {
	svc *ServiceClient
}

func InitPermissionMiddleware(svc *ServiceClient) PermissionMiddlewareConfig {
	return PermissionMiddlewareConfig{svc}
}

func (c *PermissionMiddlewareConfig) PermissionRequired(ctx *gin.Context) {
	roleId, _ := ctx.Get("roleId")
	method := ctx.Request.Method
	route := ctx.Request.URL.Path

	fmt.Println(roleId)
	fmt.Println(method)
	fmt.Println(route)

	res, err := c.svc.Client.CheckPermission(context.Background(), &pb.CheckPermissionRequest{
		Method: method,
		Route:  route,
		RoleId: roleId.(int64),
	})

	if err != nil || res.Status != http.StatusOK {
		ctx.AbortWithStatus(http.StatusUnauthorized)
		return
	}

	ctx.Next()
}
