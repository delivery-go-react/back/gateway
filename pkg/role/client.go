package role

import (
	"fmt"

	"gitlab.com/delivery-go-react/back/gateway/config"
	"gitlab.com/delivery-go-react/back/gateway/pkg/role/pb"
	"google.golang.org/grpc"
)

type ServiceClient struct {
	Client pb.RoleServiceClient
}

func InitServiceClient(c *config.Config) pb.RoleServiceClient {
	cc, err := grpc.Dial(c.RoleSvcUrl, grpc.WithInsecure())

	if err != nil {
		fmt.Println("Could not connect:", err)
	}

	return pb.NewRoleServiceClient(cc)
}
