package routes

import (
	"context"
	"net/http"

	"github.com/gin-gonic/gin"
	"gitlab.com/delivery-go-react/back/gateway/pkg/role/pb"
)

func GetAllPermissions(ctx *gin.Context, c pb.RoleServiceClient) {
	res, err := c.GetAllPermissions(context.Background(), &pb.EmptyRequest{})

	if err != nil {
		ctx.AbortWithError(http.StatusBadGateway, err)
		return
	}

	ctx.JSON(int(res.Status), &res)
}
